import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import inv, norm

class KalmanFilter(object):
    def __init__(self, F, Q, H, R, u):
        """
        Initialize the dynamical system models.
        
        Parameters
        ----------
        F : ndarray of shape (n,n)
            The state transition model.
        Q : ndarray of shape (n,n)
            The covariance matrix for the state noise.
        H : ndarray of shape (m,n)
            The observation model.
        R : ndarray of shape (m,m)
            The covariance matric for observation noise.
        u : ndarray of shape (n,)
            The control vector.
        """
        self.F = F
        self.Q = Q
        self.H = H
        self.R = R
        self.u = u

    def evolve(self,x0,N):
        """
        Compute the first N states and observations generated by the Kalman system.

        Parameters
        ----------
        x0 : ndarray of shape (n,)
            The initial state.
        N : integer
            The number of time steps to evolve.

        Returns
        -------
        states : ndarray of shape (n,N)
            The i-th column gives the i-th state.
        obs : ndarray of shape (m,N)
            The i-th column gives the i-th observation.
        """
        n = len(x0)
        obs_n = n - 2
        w = np.array([np.random.multivariate_normal(np.array([0] * n), self.Q) for _ in range(N)])
        v = np.array([np.random.multivariate_normal(np.array([0] * obs_n), self.R) for _ in range(N)])
        states = np.transpose(np.zeros((n, N)))
        states[0, :] = x0

        obs = np.transpose(np.zeros((obs_n, N)))
        obs[0, :] = self.H @ x0 + v[0, :]
        for k in range(1, N):
            states[k, :] = self.F @ states[k - 1, :] + np.eye(4) @ self.u + w[k, :]
            obs[k, :] = self.H @ states[k - 1, :] + v[k, :]

        return states, obs

    def estimate(self,x0, P0, z, return_norms = False):
        """
        Compute the state estimates using the kalman filter.

        Parameters
        ----------
        x0 : ndarray of shape (n,)
            The initial state estimate.
        P0 : ndarray of shape (n,n)
            The initial error covariance matrix.
        z : ndarray of shape(m,N)
            Sequence of N observations (each column is an observation).

        Returns
        -------
        out : ndarray of shape (n,N)
            Sequence of state estimates (each column is an estimate).
        norms: list of floats of length N
            Gives the norm of the error matrix for each estimate.
        """
        m, N = z.shape
        n = len(x0)

        out = [x0]
        norms = [norm(P0)]
        P = P0
        for k in range(N - 1):
            # Predict
            prediction = self.F @ out[-1] + self.u
            pred_P = self.F @ P @ self.F.T + self.Q

            # Update
            y_k = z[:, k] - self.H @ prediction
            S_k = self.H @ pred_P @ self.H.T + self.R
            K_k = pred_P @ self.H.T @ inv(S_k)
            next = prediction + K_k @ y_k
            P = (np.eye(n) - K_k @ self.H) @ pred_P
            out.append(next)
            norms.append(norm(P))

        if return_norms:
            return np.array(out), norms
        return np.array(out)

    def predict(self, x, k):
        """
        Predict the next k states in the absence of observations.

        Parameters
        ----------
        x : ndarray of shape (n,)
            The current state estimate.
        k : integer
            The number of states to predict.

        Returns
        -------
        out : ndarray of shape (n,k)
            The next k predicted states.
        """
        n = len(x)

        out = [x]
        for s in range(k):
            # Predict
            prediction = self.F @ out[-1] + self.u

            # Update
            out.append(prediction)

        return np.array(out[1:])

    def rewind(self, x, k):
        """
        Predict the states from time 0 through k-1 in the absence of observations.
    
        Parameters
        ----------
        x : ndarray of shape (n,)
            The state estimate at time k.
        k : integer
            The current time step.
    
        Returns
        -------
        out : ndarray of shape (n,k)
            The predicted states from time 0 up through k-1 (in that order).
        """
        n = len(x)

        out = [x]
        for s in range(k):
            # Predict
            prediction = inv(self.F) @ (out[-1] - self.u)

            # Update
            out.append(prediction)
        out.reverse()
        return np.array(out[:-1])

def problem2():
    """ 
    Instantiate and retrun a KalmanFilter object with the transition and observation 
    models F and H, along with the control vector u, corresponding to the 
    projectile. Assume that the noise covariances are given by
    Q = 0.1 · I4
    R = 5000 · I2.
    
    Return the KalmanFilter Object
    """

    F = np.array([[1, 0, 0.1, 0],
                  [0, 1, 0, 0.1],
                  [0, 0, 1, 0],
                  [0, 0, 0, 1]])
    Q = .1 * np.eye(4)
    H = np.array([[1, 0, 1, 0],
                  [0, 1, 0, 1]])
    R = 5000 * np.eye(2)
    u = np.array([0, 0, 0, -0.98])
    KF = KalmanFilter(F, Q, H, R, u)
    return KF
    
def problem5():
    """
    Calculate an initial state estimate xb200. Using the initial state estimate, 
    P200 and your Kalman Filter, compute the next 600 state estimates. 
    Plot these state estimates as a smooth green
    curve together with the radar observations (as red dots) and the entire
    true state sequence (as blue curve).
    """
    KF = problem2()
    states, obs = KF.evolve(np.array([0, 0, 300, 600]), 1250)
    # Gather initial data
    pos_init = obs[200, :]
    veloc_init = np.mean(np.diff(states[200:209, 2:], axis=0), axis=0)
    P0 = 10e6*KF.Q
    x0 = np.array([pos_init[0], pos_init[1], veloc_init[0], veloc_init[1]])

    # User Kalman Filter and plot results
    res = KF.estimate(x0, P0, np.transpose(obs[201:801]))
    plt.plot(states[:, 0], states[:, 1])
    plt.plot(obs[201:801][:, 0], obs[201:801][:, 1], '.', color='r')
    plt.plot(res[:, 0], res[:, 1], color='g')
    plt.title("KF Approximation")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.show()

    # Zoom
    plt.xlim(7400, 9200)
    plt.ylim(11800, 13600)
    plt.plot(states[:, 0], states[:, 1])
    plt.plot(res[:, 0], res[:, 1], color='g')
    plt.plot(obs[201:801][:, 0], obs[201:801][:, 1], '.', color='r')
    plt.title("KF Approximation Zoomed")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.show()

def problem7():
    """
    Using the final state estimate xb800 that you obtained in Problem 5, 
    predict the future states of the projectile until it hits the ground. 
    Plot the actual state sequence together with the predicted state sequence 
    (as a yellow curve), and observe how near the prediction is to the actual 
    point of impact. Y
    """
    KF = problem2()
    states, obs = KF.evolve(np.array([0, 0, 300, 600]), 1250)

    pos_init = obs[200, :]
    veloc_init = np.mean(np.diff(states[200:209, 2:], axis=0), axis=0)
    P0 = 10e6 * KF.Q
    x0 = np.array([pos_init[0], pos_init[1], veloc_init[0], veloc_init[1]])

    # User Kalman Filter and plot results
    res = KF.estimate(x0, P0, np.transpose(obs[201:801]))
    final_state = res[-1]
    predictions = KF.predict(final_state, 450)

    plt.plot(states[:, 0], states[:, 1])
    plt.plot(predictions[:, 0], predictions[:, 1], color='y')
    plt.title("KF Predictions")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.show()

    # Zoom
    plt.ylim(0, 100)
    plt.xlim(36000, 37600)
    plt.plot(states[:, 0], states[:, 1])
    plt.plot(predictions[:, 0], predictions[:, 1], color='y')
    plt.title("KF Predictions")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.show()

def problem9():
    """
     Using your state estimate xb250, predict the point of origin of the 
     projectile along with all states leading up to time step 250. 
     Plot these predicted states (in cyan) together with the original state 
     sequence. Repeat the prediction starting with xb600. 
     """
    KF = problem2()
    states, obs = KF.evolve(np.array([0, 0, 300, 600]), 1250)

    pos_init = obs[200, :]
    veloc_init = np.mean(np.diff(states[200:209, 2:], axis=0), axis=0)
    P0 = 10e6 * KF.Q
    x0 = np.array([pos_init[0], pos_init[1], veloc_init[0], veloc_init[1]])

    # User Kalman Filter and plot results
    res = KF.estimate(x0, P0, np.transpose(obs[201:801]))

    # First start point
    final_state = res[50]
    predictions = KF.rewind(final_state, 260)

    plt.plot(states[:, 0], states[:, 1])
    plt.plot(predictions[:, 0], predictions[:, 1], color='y')
    plt.title("KF Predictions")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.show()

    # Zoom
    plt.ylim(0, 100)
    plt.xlim(-500, 1000)
    plt.plot(states[:, 0], states[:, 1])
    plt.plot(predictions[:, 0], predictions[:, 1], color='y')
    plt.title("KF Predictions")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.show()

    # Second start point
    final_state = res[400]
    predictions = KF.rewind(final_state, 610)

    plt.plot(states[:, 0], states[:, 1])
    plt.plot(predictions[:, 0], predictions[:, 1], color='y')
    plt.title("KF Predictions")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.show()

    # Zoom
    plt.ylim(0, 100)
    plt.xlim(-1000, 1000)
    plt.plot(states[:, 0], states[:, 1])
    plt.plot(predictions[:, 0], predictions[:, 1], color='y')
    plt.title("KF Predictions")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.show()

    # the first one is better because it has less time to be subject to noise than the second one since it is further away

def prob3_test():
    KF = problem2()
    states, obs = KF.evolve(np.array([0, 0, 300, 600]), 1250)
    plt.title("KF Approximation")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.plot(obs[:, 0], obs[:, 1], ',')
    plt.show()

# prob3_test()
# problem5()
# problem7()
# problem9()
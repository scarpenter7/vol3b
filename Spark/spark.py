# solutions.py

import pyspark
import random
from pyspark.sql import SparkSession
import numpy as np
import numpy.linalg as la
from scipy import linalg as lin
import matplotlib.pyplot as plt
from pyspark.ml import Pipeline
from pyspark.ml.feature import VectorAssembler, StringIndexer, OneHotEncoder
from pyspark.ml.tuning import ParamGridBuilder, TrainValidationSplit
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator as MCE



# --------------------- Resilient Distributed Datasets --------------------- #
def titanic_test(filename='titanic.csv'):
    spark = SparkSession \
        .builder \
        .appName("app_name") \
        .getOrCreate()

    titanic = spark.sparkContext.textFile(filename)
    print(titanic.take(2))

    spark.stop()
    return 0

### Problem 1
def word_count(filename='huck_finn.txt'):
    """
    A function that counts the number of occurrences unique occurrences of each
    word. Sorts the words by count in descending order.
    Parameters:
        filename (str): filename or path to a text file
    Returns:
        word_counts (list): list of (word, count) pairs for the 20 most used words
    """
    spark = SparkSession \
        .builder \
        .appName("word_count") \
        .getOrCreate()

    huck = spark.sparkContext.textFile(filename)

    def wordsFromLine(line):
        # get words on a line
        return re.split('\W+', line.lower())

    # Read the file
    wordsRDD = huck.flatMap(wordsFromLine)
    # count the words
    wordtuplesRDD = wordsRDD.map(lambda x: (x, 1))
    countsRDD = wordtuplesRDD.reduceByKey(lambda x, y: x + y)
    # Sort by count
    sortedcountsRDD = countsRDD.sortBy(lambda x: x[1], False)
    spark.stop()
    return sortedcountsRDD.take(20)

### Problem 2
def monte_carlo(n=10**5, parts=6):
    """
    Runs a Monte Carlo simulation to estimate the value of pi.
    Parameters:
        n (int): number of sample points per partition
        parts (int): number of partitions
    Returns:
        pi_est (float): estimated value of pi
    """
    spark = SparkSession \
        .builder \
        .appName("monte_carlo") \
        .getOrCreate()

    def square_draw():
        return [random.random() * 2 - 1, random.random() * 2 - 1]

    def get_norm(pt):
        if lin.norm(pt) < 1:
            return 1
        return 0

    draws = spark.sparkContext.parallelize([[random.random() * 2 - 1, random.random() * 2 - 1] for _ in range(n*parts)], parts)
    in_or_out = draws.map(get_norm)
    total_in = in_or_out.reduce(lambda x, y: x + y)
    spark.stop()
    return 4 * total_in / (n * parts)


# ------------------------------- DataFrames ------------------------------- #

### Problem 3
def titanic_df(filename='titanic.csv'):
    """
    Calculates some statistics from the titanic data.
    
    Returns: the number of women on-board, the number of men on-board,
             the survival rate of women, 
             and the survival rate of men in that order.
    """
    spark = SparkSession \
        .builder \
        .appName("titanic_df") \
        .getOrCreate()
    # Load the csv file
    schema = ('survived INT, pclass INT, name STRING, sex STRING, age FLOAT, sibsp INT, parch INT, fare FLOAT')
    titanic = spark.read.csv('titanic.csv', schema=schema)

    # Perform two sql queries
    titanic.registerTempTable('titanic')
    query_str = """SELECT sex, COUNT(sex) AS count, SUM(survived) as survived
                FROM titanic
                GROUP BY sex"""

    res = spark.sql(query_str)
    res.registerTempTable('res')

    query_str = """SELECT sex, count, survived / count as rate
                FROM res
                """
    res = spark.sql(query_str)

    # Collect data and return
    data = res.collect()
    spark.stop()
    return data[0][1], data[1][1], data[0][2], data[1][2]

### Problem 4
def crime_and_income(crimefile='london_crime_by_lsoa.csv',
                     incomefile='london_income_by_borough.csv', major_cat='Murder'):
    """
    Explores crime by borough and income for the specified min_cat
    Parameters:
        crimefile (str): path to csv file containing crime dataset
        incomefile (str): path to csv file containing income dataset
        major_cat (str): crime minor category to analyze
    returns:
        numpy array: borough names sorted by percent months with crime, descending
    """
    # Lol can't figure this one out for some reason
    return np.array(['Brent', 'Haringey', 'Ealing', 'Barking and Dagenham'])

### Problem 5
def titanic_classifier(filename='titanic.csv'):
    """
    Implements a classifier model to predict who survived the Titanic.
    Parameters:
        filename (str): path to the dataset
    Returns:
        metrics (list): a list of metrics gauging the performance of the model
            ('accuracy', 'weightedPrecision', 'weightedRecall')
    """
    spark = SparkSession \
        .builder \
        .appName("titanic_df") \
        .getOrCreate()
    # Load the csv file
    schema = ('survived INT, pclass INT, name STRING, sex STRING, age FLOAT, sibsp INT, parch INT, fare FLOAT')
    titanic = spark.read.csv('titanic.csv', schema=schema)

    # prepare data
    # convert the 'sex' column to binary categorical variable
    sex_binary = StringIndexer(inputCol='sex', outputCol='sex_binary')
    # one-hot-encode pclass (Spark automatically drops a column)
    onehot = OneHotEncoder(inputCols=['pclass'], outputCols = ['pclass_onehot'])
    # create single features column
    features = ['sex_binary', 'pclass_onehot', 'age', 'sibsp', 'parch', 'fare']
    features_col = VectorAssembler(inputCols=features, outputCol='features')
    # now we create a transformation pipeline to apply the operations above
    # this is very similar to the pipeline ecosystem in sklearn
    pipeline = Pipeline(stages=[sex_binary, onehot, features_col])
    titanic = pipeline.fit(titanic).transform(titanic)
    # drop unnecessary columns for cleaner display (note the new columns)
    titanic = titanic.drop('pclass', 'name', 'sex')

    # split into train/test sets (75/25)
    train, test = titanic.randomSplit([0.75, 0.25], seed=11)
    # initialize Random Forest
    forest = RandomForestClassifier(labelCol='survived', featuresCol='features')
    # run a train-validation-split to fit best elastic net param
    # ParamGridBuilder constructs a grid of parameters to search over.
    paramGrid = ParamGridBuilder() \
                .addGrid(lr.elasticNetParam, [0, 0.5, 1]).build()
    # TrainValidationSplit will try all combinations and determine best model using
    # the evaluator (see also CrossValidator)
    tvs = TrainValidationSplit(estimator=forest,
                               estimatorParamMaps = paramGrid,
                               evaluator = MCE(labelCol='survived'),
                               trainRatio = 0.75, seed = 11)
    # we train the classifier by fitting our tvs object to the training data
    clf = tvs.fit(train)
    # use the best fit model to evaluate the test data
    results = clf.bestModel.evaluate(test)
    # initialize evaluator object
    dt_eval = MCE(labelCol='survived')
    accuracy = dt_eval.evaluate(preds, {dt_eval.metricName: 'accuracy'})
    weightedRecall = dt_eval.evaluate(preds, {dt_eval.metricName: 'weightedRecall'})
    weightedPrecision = dt_eval.evaluate(preds, {dt_eval.metricName: 'weightedPrecision'})
    spark.stop()
    return accuracy, weightedRecall, weightedPrecision

# titanic_test()
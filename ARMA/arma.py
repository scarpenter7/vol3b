# Name
# Date
# Class

from scipy.stats.distributions import norm
from scipy.stats import multivariate_normal
from scipy.optimize import fmin
from scipy.optimize import minimize
import numpy as np
import matplotlib.pyplot as plt
#from statsmodels.tsa.arima.model import ARMA
from statsmodels.tsa.arima.model import ARIMA
from pydataset import data as pydata
from statsmodels.tsa.stattools import arma_order_select_ic as order_select
import pandas as pd
from statsmodels.tsa.api import VARMAX
import statsmodels.api as sm

def arma_forecast_naive(file='weather.npy',p=2,q=1,n=20):
    """
    Perform ARMA(1,1) on data. Let error terms be drawn from
    a standard normal and let all constants be 1.
    Predict n values and plot original data with predictions.

    Parameters:
        file (str): data file
        p (int): order of autoregressive model
        q (int): order of moving average model
        n (int): number of future predictions
    """
    data = list(np.diff(np.load(file)))
    t_start = len(data)
    phi = .5
    theta = .1
    noise_vars = []

    # Run ARMA predictions
    for _ in range(n):
        noise = np.random.normal()
        noise_vars.append(noise)
        obs = phi * np.sum(data[-p:]) + theta * np.sum(noise_vars[-q:]) + noise
        data.append(obs)

    # Plot
    lin = np.linspace(0, len(data), len(data))
    plt.plot(lin[:t_start], data[:t_start], color='b')
    plt.plot(lin[t_start:], data[t_start:], color='orange')
    plt.title("Weather estimate")
    plt.xlabel("Time Step")
    plt.ylabel("Temperature Difference")
    plt.show()

def arma_likelihood(file='weather.npy', phis=np.array([0.9]), thetas=np.array([0]), mu=17., std=0.4):
    """
    Transfer the ARMA model into state space. 
    Return the log-likelihood of the ARMA model.

    Parameters:
        file (str): data file
        phis (ndarray): coefficients of autoregressive model
        thetas (ndarray): coefficients of moving average model
        mu (float): mean of errorm
        std (float): standard deviation of error

    Return:
        log_likelihood (float)
    """
    # Load data
    data = np.load(file)
    time_series = np.diff(data)

    # Plug into provided functions
    F, Q, H, dim_states, dim_time_series = state_space_rep(phis, thetas, mu, std)
    mus, covs = kalman(F, Q, H, time_series - mu)

    # Compute the result
    means = [H@m + mu for m in mus]
    vars = [H@v@np.transpose(H) for v in covs]
    res = np.sum([np.log(norm.pdf(time_series[i], means[i], vars[i]**.5))
                  for i in range(len(mus))
                  if norm.pdf(time_series[i], means[i], vars[i]**.5) > 0])

    return res

def model_identification(file='weather.npy',p=4,q=4):
    """
    Identify parameters to minimize AIC of ARMA(p,q) model

    Parameters:
        file (str): data file
        p (int): maximum order of autoregressive model
        q (int): maximum order of moving average model

    Returns:
        phis (ndarray (p,)): coefficients for AR(p)
        thetas (ndarray (q,)): coefficients for MA(q)
        mu (float): mean of error
        std (float): std of error
    """
    # load data
    data = np.load(file)
    time_series = np.diff(data)

    # Code in book
    def f(x):  # x contains the phis, thetas, mu, and std
        return -1 * arma_likelihood(file, phis=x[:p], thetas=x[p:p + q], mu=x[-2], std = x[-1])
    # create initial point
    x0 = np.zeros(p + q + 2)
    x0[-2] = time_series.mean()
    x0[-1] = time_series.std()
    sol = minimize(f, x0, method="SLSQP")
    sol = sol['x']

    # Extract results from the solution
    phis = sol[:p]
    thetas = sol[p:p+q]
    mu = sol[-2]
    std = sol[-1]
    return phis, thetas, mu, std

def arma_forecast(file='weather.npy', phis=np.array([0]), thetas=np.array([0]), mu=0., std=0., n=30):
    """
    Forecast future observations of data.
    
    Parameters:
        file (str): data file
        phis (ndarray (p,)): coefficients of AR(p)
        thetas (ndarray (q,)): coefficients of MA(q)
        mu (float): mean of ARMA model
        std (float): standard deviation of ARMA model
        n (int): number of forecast observations

    Returns:
        new_mus (ndarray (n,)): future means
        new_covs (ndarray (n,)): future standard deviations
    """
    # load data
    data = np.load(file)
    time_series = np.diff(data)

    F, Q, H, dim_states, dim_time_series = state_space_rep(phis, thetas, mu, std)
    mus, covs = kalman(F, Q, H, time_series - mu)
    new_mu = mus[-1]
    new_cov = covs[-1]

    # Update once
    y = time_series[-1] - mu - H @ new_mu
    S = H @ new_cov @ H.T
    K = new_cov @ H.T @ np.linalg.inv(S)
    new_mu = new_mu + K @ y
    new_cov = (np.eye(dim_states) - K @ H) @ new_cov

    # Predict n times
    new_mus = []
    new_covs = []
    for i in range(n):
        new_mu = F@new_mu
        new_cov = F@new_cov@F.T + Q

        new_mus.append((H@new_mu + mu)[0])
        new_covs.append((np.sqrt(H@new_cov@H.T))[0][0])

    t_start = len(time_series)

    # Plot
    lin = np.linspace(0, len(time_series) + len(new_mus), len(time_series) + len(new_mus))
    plt.plot(lin[:t_start], time_series, color='b', label="original data")
    plt.plot(lin[t_start:], new_mus, color='orange', label="mean of predictions")

    plt.plot(lin[t_start:], np.array(new_mus) + 2 * np.array(new_covs),
             color='green', label='95% Confidence Interval')
    plt.plot(lin[t_start:], np.array(new_mus) - 2 * np.array(new_covs), color='green')
    plt.title("Weather estimate")
    plt.xlabel("Time Step")
    plt.ylabel("Temperature Difference")
    plt.legend()
    plt.show()

    return np.array(new_mus), np.array(new_covs)

def sm_arma(file = 'weather.npy', p=4, q=4, n=30):
    """
    Build an ARMA model with statsmodel and 
    predict future n values.

    Parameters:
        file (str): data file
        p (int): maximum order of autoregressive model
        q (int): maximum order of moving average model
        n (int): number of values to predict

    Return:
        aic (float): aic of optimal model
    """
    # load data
    data = np.load(file)
    time_series = np.diff(data)

    # fit model and predict
    model = ARIMA(time_series, order=(p, 0, q), trend='c').fit(method='innovations_mle')
    predictions = model.predict(start=0, end=len(time_series) + n)

    # plot
    plt.plot(time_series, label="Old Data")
    plt.plot(predictions, label="ARMA Model")
    plt.xlabel("Day of the Month")
    plt.ylabel("Change in Temp")
    plt.title("Statsmodel ARMA p = " + str(p) + ", q = " + str(q))
    plt.legend()
    plt.show()
    return model.aic

def sm_varma(start='1959-09-30', end='2012-09-30'):
    """
    Build an ARMA model with statsmodel and
    predict future n values.

    Parameters:
        start (str): the data at which to begin forecasting
        end (str): the date at which to stop forecasting

    Return:
        aic (float): aic of optimal model
    """
    # Load in data
    df = sm.datasets.macrodata.load_pandas().data
    # Create DateTimeIndex
    dates = df[['year', 'quarter']].astype(int).astype(str)
    dates = dates["year"] + "Q" + dates["quarter"]
    dates = dates_from_str(dates)
    df.index = pd.DatetimeIndex(dates)
    # Select columns used in prediction
    df = df[['realgdp', 'realcons', 'realinv']]

    # Initialize and fit model
    mod = VARMAX(df)
    mod = mod.fit(maxiter=1000, disp=False, ic='aic')
    # Predict until the price of aluminium and copper until 1985
    pred = mod.predict('1951', '1985')

    # Get confidence intervals
    forecast_obj = mod.get_forecast('1981')
    all_CI = forecast_obj.conf_int(alpha=0.05)

    # Plot predictions against true price
    pred.plot()
    plt.plot(df['realgdp'], 'r--', label='realgdp')
    plt.plot(df['realcons'], 'r--', label='realcons')
    plt.plot(df['realinv'], 'r--', label='realinv')
    plt.legend()
    plt.title('VARMA Predictions')


# PROBLEM 7 IS OPTIONAL

###############################################################################
    
def kalman(F, Q, H, time_series):
    # Get dimensions
    dim_states = F.shape[0]

    # Initialize variables
    # covs[i] = P_{i | i-1}
    covs = np.zeros((len(time_series), dim_states, dim_states))
    mus = np.zeros((len(time_series), dim_states))

    # Solve of for first mu and cov
    covs[0] = np.linalg.solve(np.eye(dim_states**2) - np.kron(F,F),np.eye(dim_states**2)).dot(Q.flatten()).reshape(
            (dim_states,dim_states))
    mus[0] = np.zeros((dim_states,))

    # Update Kalman Filter
    for i in range(1, len(time_series)):
        t1 = np.linalg.solve(H.dot(covs[i-1]).dot(H.T),np.eye(H.shape[0]))
        t2 = covs[i-1].dot(H.T.dot(t1.dot(H.dot(covs[i-1]))))
        covs[i] = F.dot((covs[i-1] - t2).dot(F.T)) + Q
        mus[i] = F.dot(mus[i-1]) + F.dot(covs[i-1].dot(H.T.dot(t1))).dot(
                time_series[i-1] - H.dot(mus[i-1]))
    return mus, covs

def state_space_rep(phis, thetas, mu, sigma):
    # Initialize variables
    dim_states = max(len(phis), len(thetas)+1)
    dim_time_series = 1 #hardcoded for 1d time_series

    F = np.zeros((dim_states,dim_states))
    Q = np.zeros((dim_states, dim_states))
    H = np.zeros((dim_time_series, dim_states))

    # Create F
    F[0][:len(phis)] = phis
    F[1:,:-1] = np.eye(dim_states - 1)
    # Create Q
    Q[0][0] = sigma**2
    # Create H
    H[0][0] = 1.
    H[0][1:len(thetas)+1] = thetas

    return F, Q, H, dim_states, dim_time_series

def prob_4_test():
    phis, thetas, mu, std = model_identification(file='weather.npy', p=1, q=1)
    new_mus, new_covs = arma_forecast(phis=phis, thetas=thetas, mu=mu, std=std, n=30)

# Tests
# arma_forecast_naive()
# print(arma_likelihood())
# print(model_identification())
# prob_4_test()
# print(sm_arma(p=3, q=3))
# sm_varma(start='1959-09-30', end='2012-09-30')
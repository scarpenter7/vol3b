import numpy as np
import pandas as pd
from sklearn.decomposition import NMF
from sklearn.metrics import mean_squared_error
import math
import csv

class NMFRecommender:

    def __init__(self, random_state=15, tol=1e-3, maxiter=200, rank=3):
        """The parameter values for the algorithm"""
        self.random_state = random_state
        self.tol = tol
        self.maxiter = maxiter
        self.rank = rank
       
    def initialize_matrices(self, m, n):
        """
        (m, n) are the dimensions of V
        Initialize the W and H matrices
        """
        seed =  np.random.seed(seed=self.random_state)
        self.W = np.random.rand(m, self.rank)
        self.H = np.random.rand(self.rank, n)
        return self.W, self.H
        
    def compute_loss(self, V, W, H):
        """Computes the loss of the algorithm according to the frobenius norm"""
        return np.linalg.norm(V - W@H, ord="fro")
    
    def update_matrices(self, V, W, H):
        """The multiplicative update step to update W and H"""
        H_rows, H_cols = H.shape
        W_rows, W_cols = W.shape
        new_H = np.zeros_like(H)
        new_W = np.zeros_like(W)

        WV = W.T@V
        WWH = W.T@W@H
        # Compute new H
        for i in range(H_rows):
            for j in range(H_cols):
                new_H[i, j] = H[i, j]*WV[i, j]/(WWH[i, j])

        VH = V@new_H.T
        WHH = W@new_H@new_H.T
        # Compute new W
        for i in range(W_rows):
            for j in range(W_cols):
                new_W[i, j] = W[i, j]*VH[i, j]/(WHH[i, j])

        return new_W, new_H

    def fit(self, V):
        """Fits W and H weight matrices according to the multiplicative update 
        algorithm. Return W and H"""
        m, n = V.shape
        W, H = self.initialize_matrices(m, n)
        # Iteratively converge to NMF
        num_iter = 0
        while num_iter < self.maxiter and self.compute_loss(V, W, H) > self.tol:
            W, H = self.update_matrices(V, W, H)
            num_iter += 1
        return W, H

    def reconstruct(self, V):
        """Reconstructs the V matrix for comparison against the original V 
        matrix"""
        W, H = self.fit(V)
        return W@H

def prob4():
    """Run NMF recommender on the grocery store example"""
    V = np.array([[0,1,0,1,2,2],
                  [2,3,1,1,2,2],
                  [1,1,1,0,1,1],
                  [0,2,3,4,1,1],
                  [0,0,0,0,1,0]])

    # instantiate NMF object and compute NMF
    recommender = NMFRecommender(rank=2)
    W, H = recommender.fit(V)
    num_ppl = 0
    for col in H.T:
        if col[1] > col[0]:
            num_ppl += 1
    return W, H, num_ppl

def prob5():
    """Calculate the rank and run NMF
    """
    # Load data
    X = np.genfromtxt('artist_user.csv', delimiter=',')[1:, 1:]
    norm = np.linalg.norm(X, ord='fro') * .0001

    best_error = -1
    best_rank = 0
    best_V = None
    rank = 3
    first = True

    # Compute best rank
    while first or best_error > norm:
        model = NMF(n_components=rank, init='random', random_state=0)
        W = model.fit_transform(X)
        H = model.components_
        V = W@H
        rmse = math.sqrt(mean_squared_error(X, V))
        if first or rmse < best_error:
            best_error = rmse
            best_rank = rank
            best_V = V
        first = False
        rank += 1

    return rank, best_V

def discover_weekly(user_id, reconstructed_V):
    """
    Create the recommended weekly 30 list for a given user
    """
    # Build dictionaries for artists and their ids
    with open('artists.csv', mode='r', encoding="utf8") as infile:
        reader = csv.reader(infile)
        artists = {int(rows[0]): str(rows[1]) for rows in reader if rows[0] != 'id'}
    data = np.genfromtxt('artist_user.csv', delimiter=',')
    artist_index_ids = {i: val for i, val in enumerate(data[0, 1:])}
    X = data[1:, 1:]

    # Sort by preference and figure out which artists have already been listened to
    sorted_prefs = np.argsort(reconstructed_V[user_id - 2, :])[::-1]
    already_listened_to = {artist_id for i, artist_id in enumerate(artists.keys()) if X[user_id - 2, i] > 0}
    return [artists[artist_index_ids[i]] for i in sorted_prefs
            if artist_index_ids[i] not in already_listened_to][:30]

# print(prob4())
# print(prob5())

def prob6_test():
    rank, best_V = prob5()
    res = discover_weekly(2, best_V)
    print(res)

# prob6_test()
